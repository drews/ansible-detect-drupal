#!/bin/bash

export BASE_DIR=${1}

if [[ -d "${BASE_DIR}" ]]; then
  find ${BASE_DIR} -name 'settings.php' | while read settings_file ; do grep -i 'drupal' $settings_file &>/dev/null && echo -n "$settings_file::" ; done
fi
