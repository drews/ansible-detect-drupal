# Overview

Check to see if drupal is installed on your hosts

## Usage

Be sure to set your 'base\_dir' variable to the root directory of where you
want to search.  This defaults to '/srv/web'

### Basic

This will output everything including ansible messages

```
ansible-playbook ./check.yaml
```



### Only display drupal matches

This will only print out the hosts that have drupal installed

```
ansible-playbook ./check.yaml  | grep -v FAILED | grep msg
```
